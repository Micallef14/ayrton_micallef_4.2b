﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public void firstLevel()
	{
		SceneManager.LoadScene (1);
	}
	public void QuitGame()
	{
		UnityEditor.EditorApplication.isPlaying = false;
	}

}
