﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameMaster : MonoBehaviour {
	
	public int points;
	public int highscore = 0;

	public Text pointsText;
	public Text InputText;
	public Text HighScore;

	void Start()
	{
		if (PlayerPrefs.HasKey ("Score")) 
		{
			if (Application.loadedLevel == 1) {
				PlayerPrefs.DeleteKey ("Score");
				points = 0;
			} 
			else 
			{
				points = PlayerPrefs.GetInt("Score");
			}
		}
		if (PlayerPrefs.HasKey ("Highscore")) {
			highscore = PlayerPrefs.GetInt ("Highscore");
		}
	}
	void Update()
	{

		pointsText.text = ("Points: " + points);
		HighScore.text = ("HighScore: " + highscore);

	}
}
