﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public void firstLevel()
	{
		SceneManager.LoadScene (1);
	}
	public void QuitGame()
	{
		UnityEditor.EditorApplication.isPlaying = false;
	}
    public void instructions()
    {
        SceneManager.LoadScene(4);
    }
    public void back()
    {
        SceneManager.LoadScene(0);
    }
}
